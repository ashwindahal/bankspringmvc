<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>


<html>

<head>
<title>Add Account</title>

<spring:url value="resources/core/css/page-style.css" var="coreCss" />
<link href="${coreCss}" rel="stylesheet" />

</head>
<body>>
	<a href="http://www.cerotid.com" Title="Link to Cerotid Website"> <img
		src="<spring:url value="/resources/core/images/logo.png" />" />
	</a>
	<hr />


	<h2>
		<font color="#ff6600">Choose Account Type: [By Default a
			checking Account will be created]</font>
	</h2>

	<%-- <form:form method="POST"> --%>

		<input type="hidden" value="CHECK_SSN">
	
			<select name="accountType">
				<option value="1">Checking</option>
				<option value="2">Saving</option>
				<option value="3">Business Checking</option>
			</select>
			
			<br />
			<br />
			
			<label for="currency-field">Provide Opening Balance to start
				Account:<br />
			</label>
			
			<input type="text" name="amount" id="amount"
				pattern="^[+-]?[0-9]{1,3}(?:[0-9]*(?:[.,][0-9]{2})?|(?:,[0-9]{3})*(?:\.[0-9]{2})?|(?:\.[0-9]{3})*(?:,[0-9]{2})?)$"
				value="" data-type="currency" placeholder="$">
			<br />
			<br />
			
			<input type="submit" value="Submit" />
			
	<%-- </form:form> --%>
	
	<!-- Ask if this is the best way to do this -->
	<a href="" Title="Link to Homepage"><h2 align="center">Return
			to HomePage</h2> </a>

	<br />
	<br />



</body>

</html>