package com.cerotid.bank.model;


public class Transaction {
	private double amount;
	private double fee;
	private String receiverFirstName;
	private String receiverLastname;
	
	
	
	
	
	public Transaction() {
	
	}

	public Transaction(double amount, double fee, String receiverFirstName, String receiverLastname) {
		this.amount = amount;
		this.fee = fee;
		this.receiverFirstName = receiverFirstName;
		this.receiverLastname = receiverLastname;
	}

	//Accessor's and Mutators 
	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public double getFee() {
		return fee;
	}

	public void setFee(double fee) {
		this.fee = fee;
	}

	public String getReceiverFirstName() {
		return receiverFirstName;
	}

	public void setReceiverFirstName(String receiverFirstName) {
		this.receiverFirstName = receiverFirstName;
	}

	public String getReceiverLastname() {
		return receiverLastname;
	}

	public void setReceiverLastname(String receiverLastname) {
		this.receiverLastname = receiverLastname;
	}

	@Override
	public String toString() {
		return "Transaction [amount=" + amount + ", fee=" + fee + ", receiverFirstName=" + receiverFirstName
				+ ", receiverLastname=" + receiverLastname + "]";
	}
	
	
}
