package com.cerotid.bank.model;

public enum AccountType {
	CHECKING, SAVING, BUSINESS_CHECKING
}
