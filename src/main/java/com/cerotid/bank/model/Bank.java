package com.cerotid.bank.model;

import java.io.Serializable;
import java.util.ArrayList;

public class Bank implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -4253737607919502883L;
	private final String bankName = "Cerotid Bank";
	private ArrayList<Customer> customers;

	// Required Accessor methods and Modifier Method
	// getters

	// so that if we make a new bank it should start with no data inside
	public Bank() {
		if (this.customers == null)
			this.customers = new ArrayList<>();
	}

	public ArrayList<Customer> getCustomers() {
		return customers;
	}

	public void setCustomers(ArrayList<Customer> customers) {
		this.customers = customers;
	}

	// only need a getBankname method because we don't need to change the bank name
	public String getBankname() {
		return bankName;
	}

	// behaviors
	public void printBankName() {
		System.out.println(getBankname()); // using the getBankname method in the class

	}

	public void printBankDetails() {
		System.out.println(toString());
	}

	
	@Override
	public String toString() {
		return "Bank [bankName=" + bankName + ", customers=" + customers + "]";
	}

	public Customer getCustomer(String ssn) {
		for (Customer customer : customers) {
			if (ssn.equals(customer.getSsn())) {
				return customer;
			}
		}
		return null;
	}

}
