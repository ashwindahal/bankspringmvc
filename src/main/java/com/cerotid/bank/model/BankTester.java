package com.cerotid.bank.model;

import java.io.IOException;
import java.util.ArrayList;

public class BankTester {

	public static void main(String[] args) throws IOException {
			/*//made bank object 
			Bank bank = new Bank();
			Bank bank2 = new Bank();
				
			//created 3 accounts types for customer 1  
			Account customer1acct1 = new Account();
			customer1acct1.setAccountType(AccountType.CHECKING);
			Account customer1acct2 = new Account();
			customer1acct2.setAccountType(AccountType.BUSINESS_CHECKING);
			Account customer1acct3 = new Account();
			customer1acct3.setAccountType(AccountType.SAVING);
			
			//created 3 accounts types for customer 2  
			Account customer2acct1 = new Account();
			customer2acct1.setAccountType(AccountType.CHECKING);
			Account customer2acct2 = new Account();
			customer2acct2.setAccountType(AccountType.BUSINESS_CHECKING);
			Account customer2acct3 = new Account();
			customer2acct3.setAccountType(AccountType.SAVING);
			
			//made a ArrayList of type Account and added each account type to that list for customer 1
			ArrayList<Account> customer1Accounts = new ArrayList<>();
			customer1Accounts.add(customer1acct1);
			customer1Accounts.add(customer1acct2);
			customer1Accounts.add(customer1acct3);
			
			//made a ArrayList of type Account and added each account type to that list for customer 2
			ArrayList<Account> customer2Accounts = new ArrayList<>();
			customer2Accounts.add(customer2acct1);
			customer2Accounts.add(customer2acct2);
			customer2Accounts.add(customer2acct3);
			
			
			//customer 1 details
			Customer customer1 = new Customer();
			customer1.setAddress("111 Main st, NY, 12345");
			customer1.setFirstName("David");
			customer1.setLastName("Smith");
			customer1.setAccounts(customer1Accounts);
			
			//customer 2 details
			Customer customer2 = new Customer();
			customer2.setAddress("321 main st, Tx, 32456");
			customer2.setFirstName("Steven");
			customer2.setLastName("Thomas");
			customer2.setAccounts(customer2Accounts);

		
			
			//added details of customer 1 in the Customer list
			ArrayList<Customer> customerList1 = new ArrayList<>(); 
			customerList1.add(customer1);
			
			//added details of customer 2 in the Customer list
			ArrayList<Customer> customerList2 = new ArrayList<>();
			customerList2.add(customer2);
			
			
			
			//customerList.add(customer3);

		
			bank.setCustomers(customerList1);
			bank2.setCustomers(customerList2);

			bank.printBankDetails();
			bank.printBankName();
			
			System.out.println();
			
			bank2.printBankDetails();
			bank2.printBankName();
			System.out.println();
			
			
			CustomerReader csr = new CustomerReader();
			csr.readFile(); 
			csr.writeFile();
			
		Testing The overloaded constructor from class CustomerReader that passes (String fileName) 
		 * 
		 * CustomerReader csr2 = new CustomerReader("RandomNames.txt"); csr2.readFile();
		 * csr2.writeFile();
		 
			
			 
	}*/

}}
