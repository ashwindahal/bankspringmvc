package com.cerotid.bank.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataAccessException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Component;

import com.cerotid.bank.model.Account;
import com.cerotid.bank.model.Address;
import com.cerotid.bank.model.Customer;

@Component
public class BankDAOImpl implements BankDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;
	/*
	 * @Value("${addCustomer.sql}") private String addCustomerSQL;
	 */

	public void addCustomer(Customer customer) {
		System.out.println("Adding customer for ssn " + customer.getSsn());
		String addCustomerSQL = "insert into customer (firstName, lastName, ssn) values (?,?,?)";

		Object[] customerParams = { customer.getFirstName(), customer.getLastName(), customer.getSsn() };

		int affectedRowsCount = jdbcTemplate.update(addCustomerSQL, customerParams);

		System.out.println(addCustomerSQL);

		String customerId = null;
		if (affectedRowsCount > 0) {
			customerId = getCustomerID(customer.getSsn());
		}

		System.out.println("CustomerId: " + customerId);

		if (customerId != null) {
			String addAddressSQL = "insert into address (addressLine, city, zipCode, stateCode, customer_id) values(?,?,?,?,?)";

			Object[] addressParams = { customer.getAddress().getAddressLine(), customer.getAddress().getCity(),
					customer.getAddress().getZipCode(), customer.getAddress().getStateCode(), customerId };

			jdbcTemplate.update(addAddressSQL, addressParams);
		}

	}

	public String getCustomerID(String ssn) {
		String sql = "select id from customer c  WHERE c.ssn = ?";

		String customerId = (String) jdbcTemplate.queryForObject(sql, new Object[] { ssn }, String.class);

		return customerId;
	}

	@Override
	public List<Customer> getCustomers() {

		String getCustomersSQL = "select * from customer c join address a on c.id = a.customer_id";
		List customerList = jdbcTemplate.query(getCustomersSQL, new ResultSetExtractor<List>() {

			public List<Customer> extractData(ResultSet rs) throws SQLException, DataAccessException {
				List<Customer> customerList = new ArrayList<Customer>();
				while (rs.next()) {

					Address address = new Address(rs.getString("addressLine"), rs.getString("city"),
							rs.getString("zipCode"), rs.getString("stateCode"));

					Customer cus = new Customer(rs.getString("firstName"), rs.getString("lastName"),
							rs.getString("ssn"), address);
					customerList.add(cus);
				}
				return customerList;
			}
		});

		System.out.println(customerList);
		return customerList;
	}

	@Override
	public boolean checkSsn(String ssn) {

		try {
			boolean ssnExists = false;
			String ssnSql = "select ssn from customer c  WHERE c.ssn = ?";

			Integer counter = jdbcTemplate.queryForObject(ssnSql, new Object[] { ssn }, Integer.class);

			if (counter > 0) {
				ssnExists = true;
			}
			return ssnExists;

		} catch (EmptyResultDataAccessException e) {
			System.out.println("Error occured while checking Customer exists" + e.getMessage());
			e.printStackTrace();


			return false;
		} catch (Exception e) {
			System.out.println("Error occured while checking Customer exists" + e.getMessage());
			e.printStackTrace();

			// TODO apply custome Exception here
			return false;
		}

	}

	@Override
	public void openAccount(Customer customer, Account account) {
		System.out.println("Adding Account");
		String addAccountSQL = "insert into account (accountType, account_open_date, amount, customerID) values (?,?,?,?)";

		String customerId = null;
		customerId = getCustomerID(customer.getSsn());
		System.out.println("CustomerId: " + customerId);

		Object[] accountParams = { String.valueOf(account.getAccountType()), account.getAccountOpenDate(), account.getAmount(),
				customerId };

		jdbcTemplate.update(addAccountSQL, accountParams);

	}
}
