package com.cerotid.bank.dao;

import java.util.List;

import com.cerotid.bank.model.Account;
import com.cerotid.bank.model.Customer;

public interface BankDAO {
	
	public void addCustomer(Customer customer);
	
	public void openAccount(Customer customer, Account account);

	public List<Customer> getCustomers(); 
	
	public boolean checkSsn(String ssn);
	

}
