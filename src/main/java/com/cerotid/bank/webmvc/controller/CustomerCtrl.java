package com.cerotid.bank.webmvc.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.cerotid.bank.bo.BankBO;
import com.cerotid.bank.model.Account;
import com.cerotid.bank.model.AccountType;
import com.cerotid.bank.model.Address;
import com.cerotid.bank.model.Customer;

@Controller
@RequestMapping("/")
public class CustomerCtrl {

	@Autowired
	@Qualifier("bankBOImpl")
	private BankBO bankBO;

	@RequestMapping(method = RequestMethod.GET)
	public String HomePage(ModelMap model) {
		return "homepage";
	}

	@RequestMapping(value = "/add-customer-form", method = RequestMethod.GET)
	public ModelAndView customer() {
		return new ModelAndView("add-customer-form", "command", new Customer());

	}

	@RequestMapping(value = "/addCustomer", method = RequestMethod.POST)
	public String addCustomer(Customer customer, Address address, ModelMap model) {

		model.addAttribute("firstName", customer.getFirstName());
		model.addAttribute("lastName", customer.getLastName());
		model.addAttribute("ssn", customer.getSsn());
		model.addAttribute("addressLine", address.getAddressLine());
		model.addAttribute("city", address.getCity());
		model.addAttribute("zipCode", address.getZipCode());
		model.addAttribute("stateCode", address.getStateCode());

		// setting the customers address and placing the customer in a list
		customer.setAddress(address);
		bankBO.addCustomer(customer);

		// adding customerList to the model so we can pass it to the view
		model.addAttribute("customerList", bankBO.retrieveCustomers());

		return "customer-display-page";

	}

	@RequestMapping(value = "/add-customer-ssn-validation", method = RequestMethod.GET)
	public String addAccountSsnValidation(ModelMap model) {

		return "add-customer-ssn-validation";

	}

	@RequestMapping(value = "/addAccount", method = RequestMethod.POST)
	public String addAccountVerification(@RequestParam("ssn") String ssn, ModelMap model) {
		// model.addAttribute("ssn", ssn);

		System.out.println("Customer Ssn: " + ssn);

		bankBO.checkSsn(ssn);

		// if ssn exists in the database return add customer page
		// else if customer does not exists in the database return customer does not

		// exists page

		if (bankBO.checkSsn(ssn) == true) {

			// send to the Add account page (view)
			return "add-account";

		}
		return "customerErrorPage";

	}

	@RequestMapping(value = "/addAccountToBank", method = RequestMethod.POST)
	public String addAccountToBank(String accountType, Customer customer, double amount, ModelMap model) {

		AccountType accountTypeSet = null;
		Account account = new Account();

		model.addAttribute("accountType", account.getAccountType());
		model.addAttribute("amount", amount);
		model.addAttribute("dateOpend", account.getDate());
		model.addAttribute("ssn", customer.getSsn());

		System.out.println("Opening Balance Amount: " + amount);

		System.out.println("AccountType Choice: " + accountType);

		if (accountType.equals("1")) {
			accountTypeSet = AccountType.CHECKING;
		} else if (accountType.equals("2")) {
			accountTypeSet = AccountType.SAVING;
		} else if (accountType.equals("3")) {
			accountTypeSet = AccountType.BUSINESS_CHECKING;
		} else {
			accountTypeSet = AccountType.CHECKING;
		}
		
		System.out.println("AccountType: " + accountTypeSet);
		
		
		
		//adding to show in view 
		model.addAttribute("accountType", accountTypeSet);
		model.addAttribute("dateOpend", new Date());
		
		
	

		account.setAccountType(accountTypeSet);
		account.setAccountOpenDate(new Date());
		account.setAmount(amount);


		System.out.println(account);
		System.out.println(customer.getSsn());

		// add account to the database
		bankBO.openAccount(customer, account);

		// send back to main page
		return "account-display-page";

	}

	// ask why ssn is not returning false
	// ask how to display customer from backend properly
	// ask why Error: "The server cannot or will not process the request due to
	// something that
	// is perceived to be a client error (e.g., malformed request syntax, invalid
	// request message
	// framing, or deceptive request routing).

}