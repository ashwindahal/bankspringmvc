package com.cerotid.bank.bo;

import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cerotid.bank.dao.BankDAO;
import com.cerotid.bank.model.Account;
import com.cerotid.bank.model.Bank;
import com.cerotid.bank.model.Customer;
import com.cerotid.bank.model.ElectronicCheck;
import com.cerotid.bank.model.MoneyGram;
import com.cerotid.bank.model.Transaction;
import com.cerotid.bank.model.WireTransfer;

@Component
public class BankBOImpl implements BankBO {
	private static Bank bank;

	@Autowired
	private BankDAO bankDAO;

	// This Static Block runs only one time before creating an object-----Ask
	// Why---------------------------------------------
	static {
		bank = new Bank();
	}

	public BankBOImpl() {
		// reads bank object
		// loadBankSer();
	}

	public void loadBankSer() { // ask if its being used correctly or not
		try {
			FileInputStream fileIn = new FileInputStream("bank.ser");
			ObjectInputStream in = new ObjectInputStream(fileIn);
			bank = (Bank) in.readObject();// down casting
			in.close();
			fileIn.close();

			System.out.println(bank); // checking bank customers first thing that i put on before menu
		} catch (Exception e1) {
			e1.printStackTrace();
		}

	}

	public void testgetCustomersByTest() {
		bank.getCustomers();
	}

	@Override
	public void addCustomer(Customer customer) {
		// first we go to the bank and get the customers inside and add a object
		// customer inside it
		// bank.getCustomers().add(customer);

		// hardcore business logic here!!!

		bankDAO.addCustomer(customer);
	}

	/*
	 * @Override public boolean openAccount(Customer customer, Account account) { if
	 * (bank.getCustomers().contains(customer)) { // Checks if the customer exits
	 * int i = bank.getCustomers().indexOf(customer);
	 * 
	 * bank.getCustomers().get(i).addAccount(account); return true; } else {
	 * addCustomer(customer); openAccount(customer, account); return true; } }
	 */

	@Override
	public void sendMoney(Customer customer, Transaction transaction) {
		System.out.println("Money Sent");

		if (transaction instanceof ElectronicCheck) {

		} else if (transaction instanceof WireTransfer) {

		} else if (transaction instanceof MoneyGram) {

		}

		// Business logic goes here
		printTransactionInfo(customer, transaction);
	}

	private void printTransactionInfo(Customer customer, Transaction transaction) {
		System.out.println(customer.getFirstName() + " Sent " + transaction.getAmount() + "as transaction Type"
				+ transaction.getClass().getSimpleName());

	}

	@Override
	public void depositMoneyInCustomerAccount(Customer customer) {
		// TODO Auto-generated method stub

	}

	@Override
	public void editCustomerInfo(Customer customer) {
		// TODO Auto-generated method stub
		// obj1 = ssn,
		// obj2 == ssn
		// update old object with new Customer details
		// Hint: equals/hashcode concept comes in

	}

	@Override
	public Customer getCustomerInfo(String ssn) {
		return bank.getCustomer(ssn);
	}

	@Override
	public void printBankStatus() {
		bank.printBankDetails();
	}

	@Override // need help here
	public List<Customer> getCustomersByState(String stateCode) {

		Map<String, List<Customer>> customerStateMap = new HashMap<>();

		// customerDao.getCustomersByStateCode(stateCode);

		// customerStateMap.containsKey(arg0);

		// pseudocode
		// if customer's state is CA
		// List<Customer> caCustomer = map.get("CA");
		// if null create new List, then add customer to caCustomer List
		// else add customer to caCustomer List

		return null;

	}

	@Override
	public Bank getBank() {
		System.out.println("Returning Bank Object");
		return bank;
	}

	@Override
	public List<Customer> retrieveCustomers() {
		// TODO Auto-generated method stub
		return bankDAO.getCustomers();
	}

	@Override
	public boolean checkSsn(String ssn) {

		return bankDAO.checkSsn(ssn);
	}

	@Override
	public void openAccount(Customer customer, Account account) {
		
		bankDAO.openAccount(customer, account);
		
	}

	
	

}
