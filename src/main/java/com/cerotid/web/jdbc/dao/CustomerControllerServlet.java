package com.cerotid.web.jdbc.dao;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;

import com.cerotid.bank.model.Account;
import com.cerotid.bank.model.AccountType;
import com.cerotid.bank.model.Address;
import com.cerotid.bank.model.Customer;

/**
 * Servlet implementation class CustomerControllerServlet
 */
@WebServlet("/CustomerControllerServlet")
public class CustomerControllerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	private CustomerDbUtil customerDbUtil;

	@Resource(name = "jdbc/web_customer_tracker")
	private DataSource dataSource;

	@Override
	public void init() throws ServletException {
		super.init();

		// create our customer db util ... and pas i the conn pool. datasource
		try {
			customerDbUtil = new CustomerDbUtil(dataSource);

		} catch (Exception exe) {
			throw new ServletException(exe);
		}

	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		// list the customers ... in MVC fashion

		try {
			// read the "command" parameter in the add-customer-form jsp
			String theCommand = request.getParameter("command");

			// if the command is missing, then default to the listing customers
			if (theCommand == null) {
				theCommand = "LIST";
			}

			// route to the approrite list
			switch (theCommand) {

			case "LIST":
				listCustomers(request, response);
				break;
			case "ADD":
				addCustomer(request, response);
				break;
			case "CHECK_SSN":
				checkSsn(request, response);
				break;
			case "ADD_ACCOUNT":
				addAccount(request, response);
				break;
			default:
				listCustomers(request, response);

			}

		} catch (Exception exe) {
			throw new ServletException(exe);

		}

	}

	private void addAccount(HttpServletRequest request, HttpServletResponse response) throws Exception {
		AccountType accountTypeSet = null;
		String accountType = request.getParameter("accountType");

		if (accountType.equals("1")) {
			accountTypeSet = AccountType.CHECKING;
		} else if (accountType.equals("2")) {
			accountTypeSet = AccountType.SAVING;
		} else if (accountType.equals("3")) {
			accountTypeSet = AccountType.BUSINESS_CHECKING;
		} else {
			accountTypeSet = AccountType.CHECKING;
		}

		Customer customer = new Customer();
		customer.setSsn(request.getParameter("ssn"));

		Account account = new Account(accountTypeSet, new Date(), Integer.parseInt(request.getParameter("amount")));

		// add the new customer to the database
		customerDbUtil.addAccount(account, customer);
		// send back to main page
		listCustomers(request, response);

	}

	private void checkSsn(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String ssn = request.getParameter("ssn");

		customerDbUtil.checkSsn(ssn);

		if (customerDbUtil.checkSsn(ssn) == true) {

			// send to the Add cu page (view)
			RequestDispatcher dispatcher = request.getRequestDispatcher("add-account-to-bank.html");
			dispatcher.forward(request, response);

		} else {
			// send to the Error page (view)
			RequestDispatcher dispatcher = request.getRequestDispatcher("customerErrorPage.html");
			dispatcher.forward(request, response);
		}

	}

	private void addCustomer(HttpServletRequest request, HttpServletResponse response) throws Exception {
		String firstName = request.getParameter("firstName");
		String lastName = request.getParameter("lastName");
		String ssn = request.getParameter("ssn");

		String addressLine = request.getParameter("addressLine");
		String city = request.getParameter("city");
		String zipCode = request.getParameter("zipCode");
		String stateCode = request.getParameter("stateCode");

		Address address = new Address(addressLine, city, zipCode, stateCode);

		// create a new Customer object
		Customer theCustomer = new Customer(firstName, lastName, ssn, address);

		// add the new customer to the database
		customerDbUtil.addCustomer(theCustomer, address);

		// send back to main page
		listCustomers(request, response);

	}

	private void listCustomers(HttpServletRequest request, HttpServletResponse response) throws Exception {

		// get students from the db util
		List<Customer> customers = customerDbUtil.getCustomers();

		// add students to the request
		request.setAttribute("customerList", customers);

		// send to the JSP page (view)
		RequestDispatcher dispatcher = request.getRequestDispatcher("add-customer-Print.jsp");
		dispatcher.forward(request, response);

	}

}
