package com.cerotid.web.jdbc.dao;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import com.cerotid.bank.model.Account;
import com.cerotid.bank.model.Address;
import com.cerotid.bank.model.Customer;

public class CustomerDbUtil {

	private DataSource dataSource;

	public CustomerDbUtil(DataSource theDataSource) {
		this.dataSource = theDataSource;

	}

	public Customer getCustomerBySSN(String ssn) throws SQLException {
		// creating the database objects
		Connection myConn = null;
		PreparedStatement myStmt = null;

		try {

			// get Connection
			myConn = dataSource.getConnection();

			// convert below into prepared Statement
			String sql = "select * from customer c join address a on c.id = a.customer_id where c.ssn= " + ssn;
			myStmt = myConn.prepareStatement(sql);

			// myStmt.execute();

			return null;

		}

		finally {
			close(myConn, myStmt, null);

		}

	}

	public List<Customer> getCustomers() throws Exception {

		// creating a list of customers
		List<Customer> customers = new ArrayList<>();

		// creating the database objects
		Connection myConn = null;
		Statement myStmt = null;
		ResultSet myRs = null;

		try {
			// get Connection
			myConn = dataSource.getConnection();

			// create sql statement
			// String sql = "select * from customer c join address a on c.id =
			// a.customer_id";

			String sql = "select * from customer c join address a on c.id = a.customer_id";

			// String sql = "select * from customer";

			myStmt = myConn.createStatement();

			// execute the query
			myRs = myStmt.executeQuery(sql);

			// process the result set

			while (myRs.next()) {

				// customer fields
				String firstName = myRs.getString("firstName");
				String lastName = myRs.getString("lastName");
				String ssn = myRs.getString("ssn");

				// address fields
				String addressLine = myRs.getString("addressLine");
				String city = myRs.getString("city");
				String zipCode = myRs.getString("zipCode");
				String stateCode = myRs.getString("stateCode");

				Address address = new Address(addressLine, city, zipCode, stateCode);

				// create new customer object
				Customer tempCustomer = new Customer(firstName, lastName, ssn, address);

				// add it to the list of customers
				customers.add(tempCustomer);

			}

			return customers;

		} finally {

			// close jdbc objects
			close(myConn, myStmt, myRs);
		}

	}

	private void close(Connection myConn, Statement myStmt, ResultSet myRs) {
		try {
			if (myConn != null) {
				myConn.close();
			}
			if (myStmt != null) {
				myStmt.close();
			}
			if (myRs != null) {
				myRs.close();
			}
		} catch (Exception exe) {
			exe.printStackTrace();
		}
	}

	public void addCustomer(Customer theCustomer, Address address) throws Exception {
		Connection myConn = null;
		PreparedStatement myStmt = null;
		PreparedStatement myStmt2 = null;

		try {
			// get db connection
			myConn = dataSource.getConnection();

			// create sql for insert
			String sql = "insert into customer" + "(firstName, lastName, ssn)" + "values (?,?,?)";
			myStmt = myConn.prepareStatement(sql);

			// set the param for the customer
			myStmt.setString(1, theCustomer.getFirstName());
			myStmt.setString(2, theCustomer.getLastName());
			myStmt.setString(3, theCustomer.getSsn());

			// execute sql insert
			int affectedRowsCount = myStmt.executeUpdate();

			int customerId = 0;

			if (affectedRowsCount > 0) {
				customerId = getCustomerID(theCustomer.getSsn());
			}

			System.out.println("CustomerId: " + customerId);

			// -------------------------------------------------------
			if (customerId > 0) {
				String sql2 = "insert into address" + " (addressLine, city, zipCode, stateCode, customer_id)"
						+ " values(?,?,?,?,?)";
				myStmt2 = myConn.prepareStatement(sql2);

				myStmt2.setString(1, address.getAddressLine());
				myStmt2.setString(2, address.getCity());
				myStmt2.setString(3, address.getZipCode());
				myStmt2.setString(4, address.getStateCode());
				myStmt2.setInt(5, customerId);

				myStmt2.execute();
			}
		} finally {
			close(myConn, myStmt, null);
			close(myConn, myStmt2, null);
		}
	}

	private int getCustomerID(String ssn) throws SQLException {
		Connection myConn = null;
		PreparedStatement myStmt = null;

		myConn = dataSource.getConnection();

		String sql = "select id from customer c where c.ssn = ?";

		myStmt = myConn.prepareStatement(sql);

		myStmt.setString(1, ssn);

		ResultSet rs = myStmt.executeQuery();

		if (rs.next()) {
			return rs.getInt("id");
		}

		return -1;
	}

	public boolean checkSsn(String ssn) throws Exception {

		boolean ssnExists = false;

		Connection myConn = null;
		PreparedStatement myStmt = null;

		myConn = dataSource.getConnection();

		String sql = "select id from customer c where c.ssn = ?";
		myStmt = myConn.prepareStatement(sql);

		myStmt.setString(1, ssn);

		ResultSet rs = myStmt.executeQuery();

		if (rs.next()) {
			ssnExists = true;
			close(myConn, myStmt, null);
		}
		return ssnExists;
	}

	private static java.sql.Date getCurrentDate() {
		java.util.Date today = new java.util.Date();
		return new java.sql.Date(today.getTime());
	}

	public void addAccount(Account account, Customer customer) throws SQLException {
		Connection myConn = null;
		PreparedStatement myStmt = null;

		try {
			// get db connection
			myConn = dataSource.getConnection();

			// create sql for insert
			String sql = "insert into account" + "(date_open, amount,customerId, accountType)" + "values (?,?,?,?)";
			myStmt = myConn.prepareStatement(sql);

			// set the param for the customer
			myStmt.setDate(1, getCurrentDate());
			myStmt.setDouble(2, account.getAmount());
			myStmt.setString(3, null);
			myStmt.setString(4, account.getAccountType().name());

			myStmt.execute();

		} finally {
			close(myConn, myStmt, null);
		}

	}
}