<%@page contentType="text/html;charset = UTF-8" language="java"%>
<%@page isELIgnored="false"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@page import="com.cerotid.bank.model.*"%>
<%@page import="java.util.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<html>
<head>
<title>Account Details</title>

<spring:url value="resources/core/css/page-style.css" var="coreCss" />
<link href="${coreCss}" rel="stylesheet" />

</head>


<body>

	<center><a href="http://www.cerotid.com" Title="Link to Cerotid Website"> <img
		src="<spring:url value="/resources/core/images/logo.png"/>" />
	</a></center>
	<hr />



	<h1 align="center">Customer Account Details</h1>


	<table border="2" width="700" align="center">
		<tr bgcolor="#ff6600">
			<th><b>Account Type</b></th>
			<th><b>Account Open Date</b></th>
		<!-- 	<th><b>Account Close Date</b></th> -->
			<th><b>Amount</b></th>
		</tr>
		<%-- Fetching the attributes of the request object 
             which was previously set by the servlet  
              "AddCustomerServlet.java" 
        --%>



		<%-- Arranging data in tabular form 
        --%>
		<tr>
			<td>${accountType}</td>
			<td>${dateOpend}</td>
			<!-- <td>NULL</td> -->
			<td>${amount}</td>
		</tr>

	</table>
	
	<br/>
	<br/>

	<!-- Ask if this is the best way to do this -->

	<!-- Ask if this is the best way to do this -->
	<center><a href="" Title="Link to Homepage"><h2>Return to HomePage</h2> </a></center>

	<br />
	<br />

</body>

</html>