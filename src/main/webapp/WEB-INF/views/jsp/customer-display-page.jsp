<%@page contentType="text/html;charset = UTF-8" language="java"%>
<%@page isELIgnored="false"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@page import="com.cerotid.bank.model.*"%>
<%@page import="java.util.*"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>


<html>
<head>
<title>Spring MVC Form Handling</title>

<spring:url value="resources/core/css/page-style.css" var="coreCss" />
<link href="${coreCss}" rel="stylesheet" />

</head>


<body>

	<center>
		<a href="http://www.cerotid.com" Title="Link to Cerotid Website">
			<img src="<spring:url value="/resources/core/images/logo.png"/>" />
		</a>
	</center>
	<hr />



	<h1 align="center">Customer List</h1>


	<table border="2" width="700" align="center">
		<tr bgcolor="#ff6600">
			<th><b>First Name</b></th>
			<th><b>Last Name</b></th>
			<th><b>SSN</b></th>
			<th><b>Address</b></th>
		</tr>
		<%-- Fetching the attributes of the request object 
             which was previously set by the servlet  
              "AddCustomerServlet.java" 
        --%>

		<c:forEach var="customers" items="${customerList}">

			<%-- Arranging data in tabular form 
        --%>
			<tr>
				<td>${customers.firstName}</td>
				<td>${customers.lastName}</td>
				<td>${customers.ssn}</td>
				<td>${customers.address}</td>
				

			</tr>
		</c:forEach>
	</table>

	<!-- Ask if this is the best way to do this -->

	<!-- Ask if this is the best way to do this -->
	<a href="add-customer-form" Title="Link to Add Customer">
		<h2>Add more Customers</h2>
	</a>
	<a href="" Title="Link to Homepage"><h2>Return to HomePage</h2> </a>

	<br />
	<br />

</body>

</html>